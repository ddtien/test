import React from 'react';
import { connect } from 'react-redux';
import Item from './Item';

class Counter extends React.Component {
  increment = () => {
    alert(this.nameValue.value);
  }
  render() {
    return (
      <div>
        <h2>List</h2>
        <input ref={el => this.nameValue=el} />
        <button onClick={this.increment}>add</button>
        <div>
        <Item />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    count: state.count
  };
}

export default connect(mapStateToProps)(Counter);
