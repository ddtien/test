import React from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import {deleteTask} from '../actions/';

class Task extends React.Component {
  render() {
    return (
      <div>
          {this.props.task.text}{this.props.task.id}
      </div>
    )
  }
}

function mapDispatchToProps(dispatch){
    return bindActionCreators({deleteTask}, dispatch);
}

export default connect(()=> mapDispatchToProps)(Task);
