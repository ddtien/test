import React from 'react';
import { connect } from 'react-redux';

class Item extends React.Component {
  decrement = () => {
    this.props.dispatch({ type: 'DECREMENT' });
  }

  render() {
    return (
      <div>
          <span>{this.props.count}</span>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    count: state.count
  };
}

export default connect(mapStateToProps)(Item);
