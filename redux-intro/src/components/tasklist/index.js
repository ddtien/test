import React from 'react';
import { connect } from 'react-redux';
import Task from '../../task';

class Tasklist extends React.Component {
  render() {
    console.log(this.props.tasks);
    return (
      <div>
          {
            this.props.tasks.map((task, index)=> <Task key={index} task={task} />)
          }
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
      tasks: state.tasks
  };
}

export default connect(mapStateToProps)(Tasklist);
