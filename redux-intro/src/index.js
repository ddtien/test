import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import App from './app';
import reducers from "./reducers/";
const store = createStore(reducers);

const Index = () => (
  <Provider store={store}>
    <App/>
  </Provider>
);

render(<Index />, document.getElementById('root'));
