import {combineReducers} from 'redux';
let nextTodoId = 0
const tasksReducer =(state=[], action)=>{
  switch(action.type){
    case 'ADD_TASK':
    return [
      ...state,
      {
        text: action.text,
        id: nextTodoId++,
        completed: false
      }
    ]
      break;
    case 'DELETE_TASK':
        state= state.concat(action.payload);
        break;
  }
  return state;
},
reducers = combineReducers({
    tasks: tasksReducer
});
export default reducers;
