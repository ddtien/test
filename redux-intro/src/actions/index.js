export const  addTask =(text)=>{
    return {
      type: 'ADD_TASK',
      text
    };
};
 export const deleteTask=(taskId)=>{
  return {
    type: 'DELETE_TASK',
    payload: taskId
  };
};
